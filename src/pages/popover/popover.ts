// import { SignupPage } from "./../signup/signup";
import { LoginPage } from "./../login/login";
import { ProfilePage } from "./../profile/profile";
// import{ SignupPage}

import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
// import{PopoverPage}
/**
 * Generated class for the PopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-popover",
  templateUrl: "popover.html"
})
export class PopoverPage {
  data: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController
  ) {}
  article() {
    this.viewCtrl.dismiss();

    // this.navCtrl.push("LoginPage");
    this.navCtrl.push("AddArticlePage");
  }

  logout() {
    this.viewCtrl.dismiss();

    this.navCtrl.setRoot(LoginPage);

    // this.navCtrl.push("AddArticlePage");
  }

  viewProfile() {
    this.navCtrl.push("ViewprofilePage");
    // this.data = this.navParams.get("data");
    // console.log(this.data);
  }
}
