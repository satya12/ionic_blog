import { LoginPage } from "./../login/login";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController
} from "ionic-angular";
import { User } from "../../app/models/user";
import { AngularFireAuth } from "angularfire2/auth";
import {
  FormBuilder,
  FormGroup,
  Validators,
  AbstractControl
} from "@angular/forms";
// import { LoginPage } from "../login/login";
// import{LoginPage}
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-signup",
  templateUrl: "signup.html"
})
export class SignupPage {
  user = {} as User;
  formgroup: FormGroup;
  email: AbstractControl;
  userName: AbstractControl;
  password: AbstractControl;
  imageLink: AbstractControl;
  constructor(
    private ngAuth: AngularFireAuth,
    formbuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    private toast: ToastController
  ) {
    // let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    this.formgroup = formbuilder.group({
      email: ["", Validators.required],

      userName: ["", Validators.required, Validators.maxLength[12]],
      password: ["", Validators.required],
      imageLink: ["", Validators.required]
    });

    this.email = this.formgroup.controls["email"];
    this.userName = this.formgroup.controls["userName"];
    this.password = this.formgroup.controls["password"];
    this.imageLink = this.formgroup.controls["imageLink"];
  }

  async signup(user: User) {
    // console.log(user);
    let data = {
      email: user.email,
      userName: user.userName
    };

    console.log(data);
    try {
      const result = await this.ngAuth.auth.createUserWithEmailAndPassword(
        user.email,
        user.password
      );
      if (result) {
        this.navCtrl.setRoot("LoginPage");
        this.toast
          .create({
            message: "successfully registered",
            duration: 3000,
            position: "bottom"
          })
          .present();
      }
      // console.log(user.email);
      console.log(result);
    } catch (e) {
      console.error(e);
    }
  }
}
