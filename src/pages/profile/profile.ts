import { EditPage } from "./../edit/edit";
import { PopoverPage } from "./../popover/popover";
// import { ProfilePage } from "./profile";
import { Component } from "@angular/core";

import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  ViewController
} from "ionic-angular";

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-profile",
  templateUrl: "profile.html"
})
export class ProfilePage {
  constructor(
    private navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController
  ) {}

  editArt() {
    this.navCtrl.push("EditPage");
  }

  showpopdown(newevent) {
    let popover = this.popoverCtrl.create(PopoverPage);

    popover.present({
      ev: newevent
    });
  }
}
