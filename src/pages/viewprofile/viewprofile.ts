// import { SignupPage } from "./../signup/signup";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
// import{SignupPage}
/**
 * Generated class for the ViewprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-viewprofile",
  templateUrl: "viewprofile.html"
})
export class ViewprofilePage {
  constructor(private navCtrl: NavController, private navParams: NavParams) {}
}
