export interface User {
  email: string;
  imageLink: string;
  password: string;
  userName: string;
}
